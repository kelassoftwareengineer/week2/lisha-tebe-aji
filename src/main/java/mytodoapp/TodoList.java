package mytodoapp;

import java.util.ArrayList;

public class TodoList {
    private ArrayList<Todo> todolist;

    public TodoList(){
        todolist = new ArrayList<Todo>();
    }

    public void add(String name, String desc, String status){
        Todo todo = new Todo(name, desc, status);
        todolist.add(todo);
    }

    public void add(Todo todo){
        todolist.add(todo);
    }

    public Todo get(int index){
        return todolist.get(index);
    }

    public String printAllTodo(){
        String out = "";
        for(Todo todo : todolist){
            out += todo.printTodoText() + "\n";
        }

        return out;
    }

	public char[] getGreeting() {
		return null;
	}     
}