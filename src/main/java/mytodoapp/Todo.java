package mytodoapp;

public class Todo{
    private String name;
    private String desc;
    private String status;
    
    public Todo(String name, String desc, String status){
        this.name = name;
        this.desc = desc;
        this.status = status;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getDesc(){
        return desc;
    }

    public void setDesc(String desc){
        this.desc = desc;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String printTodoText(){
        String output = 
        "---\n" + 
        "Name : " + name + "\n" + 
        "Description : " + desc + "\n" + 
        "Status : " + status;
        
        return output;
    }
}